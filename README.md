# SAE C Viggo Casciano Erwann Lagouche

# L'annuaire

Ce projet à été fait lors d'une SAE (situation d'apprentisage et d'évalutation) avec mon camarade Erwann Lagouche,
celui-ci etait un livrable qu'on devait donner avec une clef usb, je l'ai donc mit sur gitLab en pushant tout d'un coup.

## Modèle d'une personne dans l'annuaire

```?Prénom,Nom,?VILLE,?69003,?01.02.03.04.05,?qqchose@gmail.com,?métier```
Seuls le nom et téléphone sont obligatoires.

# Mode d'emploi

## Compilation

Compilez ce programme avec votre compilateur favori. Il a été testé sur de nombreux compilateurs, mais ```gcc``` est
cependant recommandé.

## Lancement

Au lancement, le programme va essayer de lire le fichier ```annuaire.csv```. Le programme ne peut pas fonctionner si ce
fichier ne n'existe pas dans le répertoire courant.

Une fois le fichier chargé, il va être analysé par le programme qui signalera quelconque erreur.

## Menu principal

Une fois lancé, le programme affiche le menu principal. Vous avez accès à plusieurs options.

### 1. Afficher l'annuaire

Cette option affiche le contenu complet dans l'état actuel de l'annuaire, trié ou non.

### 2. Ajouter une personne

Cette option permet d'ajouter une personne à l'annuaire.

Quand on choisit l'option, on nous demande de manière consécutive toutes les informations nécessaires. Seul le champ du
nom de la personne est obligatoire, les autres peuvent être vides, si l'utilisateur n'entre rien et appuie simplement
sur entrer. Une fois les informations entrées, on vous propose de les vérifier et de recommencer la saisie si
nécessaire. Une fois la saisie confirmée par l'utilisateur, l'entrée sera ajoutée en fin d'annuaire.

Après cette étape, il est recommandé d'utiliser l'option de tri.

### 3. Filtrer l'annuaire

Cette option permet d'appliquer un filtre à l'annuaire, pour un champ donné.

Une fois l'option choisie, on va demander à l'utilisateur quel champ filtrer. Puis on lui demande le filtre à appliquer.
Une fois filtré, on affiche le résultat et demande à l'utilisateur s'il souhaite remplacer l'annuaire actuel par le
résultat trouvé.

### 4. Trier l'annuaire

Cette option permet de trier l'annuaire par ordre alphabétique, pour un champ donné.

Une fois l'option choisie, on va demander à l'utilisateur par quel champ trier. Une fois trié, on affiche le résultat et
demande à l'utilisateur s'il souhaite remplacer l'annuaire actuel par le résultat trouvé.

### 5. Rechercher une personne

Cette option permet de rechercher une personne dans l'annuaire. Elle utilise l'algorithme de recherche de
la [distance de Levenshtein](https://fr.wikipedia.org/wiki/Distance_de_Levenshtein) pour donner un résultat
approximatif. Contrairement à l'option de filtrage, cette option est plus utile pour chercher une entrée particulière du
tableau.

Une fois l'option choisie, on va demander à l'utilisateur la recherche à effectuer. On affiche ensuite le résultat, avec
les numéros d'entrées correspondants.

### 6. Modifier ou supprimer une personne

Cette option permet à l'utilisateur de modifier ou de supprimer une entrée de l'annuaire.
*Il est grandement recommandé d'utiliser l'option n°6, "rechercher", pour trouver l'index d'une entrée de l'annuaire*.

On va d'abord demander à l'utilisateur de choisir une personne à modifier ou à supprimer, en donnant son numéro d'entrée
dans l'annuaire. Ensuite, on demande à l'utilisateur s'il souhaite la modifier ou la supprimer. S'il a choisis la
modification, on demande à l'utilisateur de donner les nouvelles données de la personne, avec le même dialogue que pour
l'option d'ajout. Sinon, elle est supprimée et le tableau est réagencé automatiquement.

### 7. Compter le nombre de personnes

Affiche le nombre d'entrées dans l'annuaire.

### 8. Afficher les utilisateurs ayant un champ manquant

Cette option va filtrer le tableau pour ne garder que les utilisateurs ayant pour lesquels on n'a pas d'informations
dans un champ donné.

On va d'abord demander à l'utilisateur de choisir le champ qu'il souhaite. Il peut choisir un champ ou tous les champs.
Une fois cela, le filtrage va être effectué automatiquement et le résultat affiché. On demande ensuite à l'utilisateur
s'il souhaite garder le résultat obtenu et remplacer l'ancien tableau.

### 9. Sauvegarder le fichier

Sauvegarde le fichier à l'emplacement où il a été chargé, ```./annuaire.csv```, en écrasant le fichier déjà présent. À
utiliser avec précaution.

### 10. Donner des informations

Affiche un petit message d'aide pour chaque option.

### 99. Quitter

Quitte le programme.