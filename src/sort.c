#include <string.h>

#include "sort.h"
#include "champ.h"

struct Personne *trierTable(struct Personne *tableau, int taille, Champ champ) {
    clock_t temps = clock();

    int i, j;
    char *k, *l;
    struct Personne temp, *p = malloc(taille * sizeof(struct Personne));
    // manipulation de la mémoire
    memcpy(p, tableau, taille * sizeof(struct Personne));

    for (i = 0; i < taille - 1; i++) {
        for (j = 0; j < taille - i - 1; j++) {
            switch (champ) {
                default:
                case PRENOM:
                    k = p[j].prenom;
                    l = p[j + 1].prenom;
                    break;
                case NOM:
                    k = p[j].nom;
                    l = p[j + 1].nom;
                    break;
                case VILLE:
                    k = p[j].ville;
                    l = p[j + 1].ville;
                    break;
                case CODE_POSTAL:
                    k = p[j].codePost;
                    l = p[j + 1].codePost;
                    break;
                case TEL:
                    k = p[j].tel;
                    l = p[j + 1].tel;
                    break;
                case EMAIL:
                    k = p[j].email;
                    l = p[j + 1].email;
                    break;
                case TRAVAIL:
                    k = p[j].travail;
                    l = p[j + 1].travail;
                    break;
            }
            if (strcmp(k, l) > 0) {
                temp = p[j];
                p[j] = p[j + 1];
                p[j + 1] = temp;
            }
        }
    }

    printf("\nExecution: %fms\n\n", (double) (clock() - temps) / CLOCKS_PER_SEC);

    return p;
}

// distance de levenshtein https://fr.wikipedia.org/wiki/Distance_de_Levenshtein
// cet algo nous donne la "distance" entière entre deux chaines de caractères,
// c'est compliqué à comprendre, mais ça fonctionne
int levenshtein(char *s1, char *s2) {
    unsigned int s1taille, s2taille, x, y, dernierDiag, ancientDiag;
    s1taille = strlen(s1);
    s2taille = strlen(s2);
    unsigned int colonne[s1taille + 1];
    for (y = 1; y <= s1taille; y++)
        colonne[y] = y;
    for (x = 1; x <= s2taille; x++) {
        colonne[0] = x;
        for (y = 1, dernierDiag = x - 1; y <= s1taille; y++) {
            ancientDiag = colonne[y];
            colonne[y] = MIN3(colonne[y] + 1, colonne[y - 1] + 1, dernierDiag + (s1[y - 1] == s2[x - 1] ? 0 : 1));
            dernierDiag = ancientDiag;
        }
    }
    return (int) colonne[s1taille];
}
