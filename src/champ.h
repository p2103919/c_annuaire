typedef enum Champ {
    PRENOM = 1,
    NOM = 2,
    VILLE = 3,
    CODE_POSTAL = 4,
    TEL = 5,
    EMAIL = 6,
    TRAVAIL = 7,
    TOUS = 0
} Champ;