/* MAIN */
#include <stdio.h>
#include <stdlib.h>

#include "sort.c"
#include "main.h"
#include "entrees.c"
#include "recherches.c"

// main
int main(void) {
    char chemin[50];
    struct Personne *table = malloc(sizeof(struct Personne)), *psTemp;
    struct Personne personne, pTemp;
    struct Recherche *rTemp;
    char *temp = malloc(1);
    char c;
    char i = 0;
    int j = 0, k, l;
    int m = 1, n = 1;  // Représente la taille de temp, vu que c'est un pointeur.
    // Menu
    int menu_i = 0, menu_j;
    int compteur;
    char input[50];  // pour les saisies au clavier

    strcpy(chemin, entreeChemin(input));

    printf("\nChargement du fichier... ");
    FILE *fichier = fopen(chemin, "r"); // ouvre le fichier
    printf("ok\n");

    printf("Verification du fichier... ");
    /* vérifie si le fichier est nul ou vide */
    if (!fichier) {
        perror("Impossible de lire le fichier");
        return EXIT_FAILURE;
    }
    printf("ok\n");

    printf("Analyse du fichier... ");
    fflush(stdout);
    // analyse du fichier en tableau de chaînes de caractères
    // (la partie difficile)

    while ((c = (char) fgetc(fichier)) != EOF) {
        // si on rencontre un retour à la ligne, on ajoute la personne
        // à la liste des personnes
        if (c == '\n') {
            strcpy(personne.travail, temp);  // ajoute le travail

            temp = memset(&temp[0], 0, n); // reinitialisation de la variable temporaire
            table = realloc(table, (j + 1) * sizeof(struct Personne));

            table[j] = personne;
            j++;
            i = 0;  // remise de l'index au début
        }
            // si c'est une virgule, on ajoute la chaîne temporaire à la propriété correspondante de la structure
        else if (c == ',') {
            switch (i) {
                case 0:
                    strcpy(personne.prenom, temp);  // ajoute le prénom
                    //printf("########## Ajout prénom: \"%s\"\n", temp);
                    break;
                case 1:
                    strcpy(personne.nom, temp);  // ajoute le nom
                    //printf("########## Ajout nom: \"%s\"\n", temp);
                    break;
                case 2:
                    strcpy(personne.ville, temp);  // ajoute la ville
                    //printf("########## Ajout ville: \"%s\"\n", temp);
                    break;
                case 3:
                    strcpy(personne.codePost, temp);  // ajoute le code postal=
                    //printf("########## Ajout code postal: \"%s\"\n", temp);
                    break;
                case 4:
                    strcpy(personne.tel, temp);  // ajoute le numéro de téléphone
                    //printf("########## Ajout tel: \"%s\"\n", temp);
                    break;
                case 5:
                    strcpy(personne.email, temp);  // ajoute l'email
                    //printf("########## Ajout email: \"%s\"\n", temp);
                    break;
                default:
                    perror("########## ERREUR D'EXÉCUTION: VALEUR DE i INVALIDE ! ##########");
                    fflush(stdout);
                    return EXIT_FAILURE;
            }
            temp = memset(&temp[0], 0, n); // reinitialisation de la variable temporaire
            m = 1;  // reinitialisation de l'index de la variable temporaire

            i++;  // incrémentation de l'index de la structure
        }
        // sinon, on ajoute le caractère à la personne
        // on exclue les \r, car les \n sont déjà gérés au-dessus
        else if (c != '\r') {
            if (n <= m + 1) {
                temp = realloc(temp, n);  // reallocation de temp
                if (temp == NULL) {
                    perror("########## Le realloc a foiré ##########\n");
                    fflush(stdout);
                    temp = malloc(n);  // si le realloc ne réussis pas, on alloue un nouvel espace mémoire.
                }
                n++;
            }

            sprintf(temp, "%s%c", temp, c);  // ajout de c en dernier caractère
            m++;
        }
    }

    printf("ok\n");

    // on ferme le fichier et on libère la mémoire
    fclose(fichier);

    while (menu_i != 9) {
        printf("\n1] Lire    - Lire le tableau entier (%d \x82l\x82ments)\n", j);  // ok
        printf("2] Ajouter - Ajouter les informations d'un utilisateur dans le tableau\n");  // ok
        printf("3] Filtrer - Filtrer les \x82l\x82ments du tableau\n");  // ok
        printf("4] Trier   - Trier les utilisateurs\n"); // ok
        printf("5] Cherch. - Rechercher un (ou plusieurs) utilisateur(s)\n");  // ok
        printf("6] Modif.  - Modifier un utilisateur ou le supprimer\n");  // ok
        printf("7] Compter - Compter le nombre d'utilisateurs dans le tableau\n");  // ok
        printf("8] Manqu.  - Afficher les utilisateurs ayant des champs manquants\n");  // ok
        printf("9] Sauv.   - Enregistrer la liste\n");  // ok
        printf("10] Infos  - En savoir plus les fonctionnalit\x82s\n");  // ok
        printf("99] Quitter\n");
        printf("\nQuelles fonctionnalit\x82s voulez-vous utiliser?: ");

        scan(input);
        menu_i = (int) strtol(input, NULL, 10);

        printf("\n");
        switch (menu_i) {
            case 1:  // AFFICHAGE TABLEAU COMPLET
                afficherTable(table, j);
                printf("\n");
                break;
            case 2:  // AJOUT UTILISATEUR
                table[j] = confirmEntree("ajouter", "ajouter au tableau");
                j++;

                break;
            case 3:  // FILTRAGE TABLEAU
                printf("Quel est le filtre que vous voulez appliquer? ");
                scan(input);
                input[strcspn(input, "\r\n")] = 0;

                // filtrage
                k = j;
                psTemp = filtrerTable(table, &k, input);

                // affichage du tableau filtré
                //printf("\n%d personnes trouvées.\n\nRésultat:\n", k);
                //afficherTable(psTemp, k);

                confirmGarder(table, &j, psTemp, k);
                break;
            case 4:  // TRIER TABLEAU
                compteur = entreeChamp("trier", 0);

                // tri
                psTemp = trierTable(table, j, compteur);

                // affichage du tableau trié
                //afficherTable(psTemp, j);

                // confirmation de la sauvegarde
                confirmGarder(table, &j, psTemp, j);
                break;
            case 5:  // RECHERCHE
                printf("Veuillez rentrer la valeur \x85 rechercher: ");

                scan(input);
                input[strcspn(input, "\r\n")] = 0;  // suppression caractère de retour en fin de ligne

                // recherche
                k = j;
                rTemp = rechercherTable(table, &k, input);

                // affichage résultat
                if (k == 0) {
                    printf("\nAucun r\x82sultat.\n\n");
                } else {
                    printf("\n");
                    //afficherRecherche(rTemp, k);
                }
                break;
            case 6:  // MODIFICATION ET SUPPRESSION
                printf("Que voulez-vous faire ?\n");
                printf("\n1] Modifier\n");
                printf("2] Supprimer\n");

                scan(input);
                compteur = (int) strtol(input, NULL, 10);

                switch (compteur) {
                    case 1:
                        printf("Veuillez rentrer l'index de la personne \x85 modifier: ");
                        scan(input);
                        compteur = (int) strtol(input, NULL, 10);
                        while (compteur < 0 || compteur > j) {
                            printf("\nEntrez un numéro de ligne valide: ");
                            scan(input);
                            compteur = (int) strtol(input, NULL, 10);
                        }
                        // entree de l'utilisateur
                        pTemp = confirmEntree("modifier", "assigner \x85 la personne actuelle");
                        // modification
                        table[compteur - 1] = pTemp;
                        break;
                    case 2:
                        printf("Veuillez rentrer l'index de la personne \x85 supprimer: ");
                        scan(input);
                        compteur = (int) strtol(input, NULL, 10);
                        // suppression
                        for (l = compteur - 1; l < j - 1; l++) {
                            table[l] = table[l + 1];
                        }
                        j--;  // décalage de la taille du tableau
                        break;
                    default:
                        printf("Abandon.");
                        break;
                }
                break;
            case 7:  // NOMBRE UTILISATEURS
                // affichage index
                printf("Il y a %d utilisateurs dans le tableau\n", j);
                break;
            case 8:  // CHAMPS MANQUANTS
                compteur = entreeChamp("v\x82rifier", 1);

                printf("Calcul des personnes avec un ou des champs manquants... ");

                // calcul
                k = j;
                rTemp = manquantsTable(table, &k, compteur);

                // affichage résultat
                if (k == 0) {
                    printf("\nAucun r\x82sultat.\n\n");
                } else {
                    printf("\n");
                    afficherRecherche(rTemp, k);
                }
                break;
            case 9:  // ENREGISTRER LE TABLEAU
                strcpy(chemin, entreeCheminSauve(input));

                fichier = fopen(chemin, "w");

                fprintf(fichier, "%s", compilerTable(table, j));
                printf("Le tableau a \x82t\x82 enregistr\x82.\n");

                fclose(fichier);
                break;
            case 10:  // AIDE ET INFOS SUR LE PROGRAMME
                menu_j = 0;
                while (menu_j != 100) {
                    printf("Sur quelle fonctionnalit\x82 vous voulez en apprendre plus? (100 pour quitter l'aide): ");

                    scan(input);
                    menu_j = (int) strtol(input, NULL, 10);

                    switch (menu_j) {
                        case 1:
                            printf("\nCette fonctionnalit\x82 va afficher le contenu du tableau\n\n");
                            break;
                        case 2:
                            printf("\nCette fonctionnalit\x82 vous permet d'ajouter un utilisateur au tableau\n");
                            break;
                        case 3:
                            printf("\nCette fonctionnalit\x82 va filtrer le tableau strictement en fonction de votre requ\x89te\n");
                            break;
                        case 4:
                            printf("\nCette fonctionnalit\x82 va trier le tableau par le param\x8Atre choisis.\n\n");
                            break;
                        case 5:
                            printf("\nCette fonctionnalit\x82 vous permet de rechercher une entr\x82e dans le tableau, à la mani\x8Are d'un moteur de recherche.\nElle utilise l'algorithme de Levenshtein.\n\n");
                            break;
                        case 6:
                            printf("\nCette fonctionnalit\x82 va modifier ou supprimer une entrée choisie du tableau.\n\n");
                            break;
                        case 7:
                            printf("\nCette fonctionnalit\x82 va afficher le nombre d'utilisateurs dans le tableau.\n\n");
                            break;
                        case 8:
                            printf("\nCette fonctionnalit\x82 va afficher les entr\x82es du tableau avec un champ manquant.\n\n");
                        case 9:
                            printf("\nCette fonctionnalit\x82 va enregistrer le fichier.\n\n");
                            break;
                        case 10:
                            printf("\nCette fonctionnalit\x82 va d\x82tailler l'utilisation d'une autre fonctionnalit\x82.\n\n");
                            break;
                        case 99:
                            printf("\n99 vous permet de quitter le programme.\n\n");
                            break;
                        case 100:
                            break;
                        default:
                            printf("\n%d n'est pas une fonctionnalité valide.\n\n", menu_j);
                            break;
                    }
                }
                printf("\n");
                break;
            case 99:
                printf("A bientôt! \n");
                return EXIT_SUCCESS;
            case 0:
                printf("Veuillez rentrer une valeur.\n");
                break;
            default:
                printf("\"%d\": Cette commande n'existe pas.\n", menu_i);
                break;
        }
    }
}

char *compilerTable(struct Personne *table, int taille) {
    char *final = malloc(taille * sizeof(struct Personne));
    int i;

    for (i = 0; i < taille; i++) {
        strcat(final, table[i].prenom);
        strcat(final, ",");
        strcat(final, table[i].nom);
        strcat(final, ",");
        strcat(final, table[i].ville);
        strcat(final, ",");
        strcat(final, table[i].codePost);
        strcat(final, ",");
        strcat(final, table[i].tel);
        strcat(final, ",");
        strcat(final, table[i].email);
        strcat(final, ",");
        strcat(final, table[i].travail);
        strcat(final, "\n");
    }

    return final;
}

void afficherTable(struct Personne *table, int taille) {
    int i;
    struct Personne p;

    for (i = 0; i < taille; i++) {
        p = table[i];
        printf("%d] %s | %s | %s | %s | %s | %s | %s\n", i + 1, p.prenom, p.nom, p.ville, p.codePost, p.tel,
               p.email,
               p.travail);
    }
}

void afficherRecherche(struct Recherche *r, int taille) {
    int i;

    for (i = 0; i < taille; i++) {
        printf("%d] %s | %s | %s | %s | %s | %s | %s\n", r[i].index,
               r[i].personne.prenom, r[i].personne.nom, r[i].personne.ville,
               r[i].personne.codePost, r[i].personne.tel, r[i].personne.email,
               r[i].personne.travail);
    }
}
