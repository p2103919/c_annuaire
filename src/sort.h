#include <time.h>
#include <stdio.h>

struct Personne {
    char prenom[50];
    char nom[50];
    char ville[50];
    char codePost[50];
    char tel[50];
    char email[50];
    char travail[50];
};

// minimum opti pour la distance de levenshtein
#define MIN3(a, b, c) ((a) < (b) ? ((a) < (c) ? (a) : (c)) : ((b) < (c) ? (b) : (c)))