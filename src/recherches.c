struct Personne *filtrerTable(struct Personne *personnes, int *taille, char *filtre) {
    clock_t temps = clock();

    struct Personne *p = malloc(*taille * sizeof(struct Personne));
    int i, j = 0;

    for (i = 0; i < *taille; i++) {
        if (strcmp(filtre, personnes[i].prenom) == 0 ||
            strcmp(filtre, personnes[i].nom) == 0 ||
            strcmp(filtre, personnes[i].ville) == 0 ||
            strcmp(filtre, personnes[i].codePost) == 0 ||
            strcmp(filtre, personnes[i].tel) == 0 ||
            strcmp(filtre, personnes[i].email) == 0 ||
            strcmp(filtre, personnes[i].travail) == 0) {
            p[j] = personnes[i];
            j++;
        }
    }

    printf("Execution: %fms\n", (double) (clock() - temps) / CLOCKS_PER_SEC);

    // changement de la valeur de la taille
    *taille = j;

    return p;
}

struct Recherche *rechercherTable(struct Personne *personnes, int *taille, char *motClef) {
    clock_t temps = clock();

    struct Recherche *r = malloc(*taille * sizeof(struct Recherche));
    int i, j = 0;

    for (i = 0; i < *taille; i++) {
        if (levenshtein(motClef, personnes[i].prenom) <= 2 ||
            levenshtein(motClef, personnes[i].nom) <= 2 ||
            levenshtein(motClef, personnes[i].ville) <= 2 ||
            levenshtein(motClef, personnes[i].codePost) <= 2 ||
            levenshtein(motClef, personnes[i].tel) <= 2 ||
            levenshtein(motClef, personnes[i].email) <= 2 ||
            levenshtein(motClef, personnes[i].travail) <= 2) {
            r[j].personne = personnes[i];
            r[j].index = i;
            j++;
        }
    }

    printf("\nExecution: %fms\n", (double) (clock() - temps) / CLOCKS_PER_SEC);

    // changement de la valeur de la taille
    *taille = j;

    return r;
}

struct Recherche *manquantsTable(struct Personne *personnes, int *taille, Champ parametre) {
    struct Recherche *r = malloc(*taille * sizeof(struct Recherche));
    int i, j = 0;

    switch (parametre) {
        case PRENOM:
            for (i = 0; i < *taille; i++) {
                if (strcmp(personnes[i].prenom, "") == 0) {
                    r[j].personne = personnes[i];
                    r[j].index = i;
                    j++;
                }
            }
            break;
        case NOM:
            for (i = 0; i < *taille; i++) {
                if (strcmp(personnes[i].nom, "") == 0) {
                    r[j].personne = personnes[i];
                    r[j].index = i;
                    j++;
                }
            }
            break;
        case VILLE:
            for (i = 0; i < *taille; i++) {
                if (strcmp(personnes[i].ville, "") == 0) {
                    r[j].personne = personnes[i];
                    r[j].index = i;
                    j++;
                }
            }
            break;
        case CODE_POSTAL:
            for (i = 0; i < *taille; i++) {
                if (strcmp(personnes[i].codePost, "") == 0) {
                    r[j].personne = personnes[i];
                    r[j].index = i;
                    j++;
                }
            }
            break;
        case TEL:
            for (i = 0; i < *taille; i++) {
                if (strcmp(personnes[i].tel, "") == 0) {
                    r[j].personne = personnes[i];
                    r[j].index = i;
                    j++;
                }
            }
            break;
        case EMAIL:
            for (i = 0; i < *taille; i++) {
                if (strcmp(personnes[i].email, "") == 0) {
                    r[j].personne = personnes[i];
                    r[j].index = i;
                    j++;
                }
            }
            break;
        case TRAVAIL:
            for (i = 0; i < *taille; i++) {
                if (strcmp(personnes[i].travail, "") == 0) {
                    r[j].personne = personnes[i];
                    r[j].index = i;
                    j++;
                }
            }
            break;
        default:
        case TOUS:
            for (i = 0; i < *taille; i++) {
                if (strcmp(personnes[i].prenom, "") == 0 ||
                    strcmp(personnes[i].nom, "") == 0 ||
                    strcmp(personnes[i].ville, "") == 0 ||
                    strcmp(personnes[i].codePost, "") == 0 ||
                    strcmp(personnes[i].tel, "") == 0 ||
                    strcmp(personnes[i].email, "") == 0 ||
                    strcmp(personnes[i].travail, "") == 0) {
                    r[j].personne = personnes[i];
                    r[j].index = i;
                    j++;
                }
            }
            break;
    }

    // changement de la valeur de la taille
    *taille = j;

    return r;
}

