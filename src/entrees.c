#include <unistd.h>

// scan l'entrée de l'utilisateur
char *scan(char *input) {
    memset(&input[0], 0, sizeof input);  // reset input
    fgets(input, INPUT_SIZE, stdin);
    return input;
}

char *entreeChemin(char *input) {
    while (1) {
        printf("\nNom du fichier: ");
        scan(input);
        input[strcspn(input, "\r\n")] = 0;
        if (strcmp(input, "") == 0) {
            printf("Entrez un chemin valide.\n");
        } else if (access(input, F_OK) == 0) {
            printf("Fichi\x82 trouv\x82.\n");
            return input;
        } else {
            printf("Impossible d'accéder au chemin du fichier. Veuillez réessayer.\n");
        }
    }
}

char *entreeCheminSauve(char *input) {
    printf("Veuillez rentrer l'emplacement de sauvegarde du fichier.\n");
    while (1) {
        printf("Nom du fichier: \n");
        scan(input);
        input[strcspn(input, "\r\n")] = 0;
        if (strcmp(input, "") == 0) {
            printf("Entrez un chemin valide.\n");
        } else {
            return input;
        }
    }
}

struct Personne entreePersonne(char *verbe) {
    struct Personne p;
    int i;
    char *input = malloc(INPUT_SIZE);

    printf("Quel prénom voulez-vous %s?: ", verbe);
    scan(input);
    input[strcspn(input, "\r\n")] = 0;  // suppression caractère de retour en fin de ligne
    strcpy(p.prenom, input);
    i = 1;
    while (i == 1) {
        printf("Quel nom voulez-vous %s? (obligatoire): ", verbe);
        scan(input);
        input[strcspn(input, "\r\n")] = 0;  // suppression caractère de retour en fin de ligne
        strcpy(p.nom, input);
        if (strcmp(p.nom, "") == 0) {
            printf("\nERREUR: Le nom est obligatoire.\n");
            fflush(stdout);
        } else {
            i = 0;
        }
    }
    printf("Quelle ville voulez-vous %s?: ", verbe);
    scan(input);
    input[strcspn(input, "\r\n")] = 0;  // suppression caractère de retour en fin de ligne
    strcpy(p.ville, input);
    printf("Quel code postal voulez-vous %s?: ", verbe);
    scan(input);
    input[strcspn(input, "\r\n")] = 0;
    strcpy(p.codePost, input);
    printf("Quelle telephone voulez-vous %s?: ", verbe);
    scan(input);
    input[strcspn(input, "\r\n")] = 0;
    strcpy(p.tel, input);
    printf("Quel email voulez-vous %s?: ", verbe);
    scan(input);
    input[strcspn(input, "\r\n")] = 0;
    strcpy(p.email, input);
    printf("Quel travail voulez-vous %s?: ", verbe);
    scan(input);
    input[strcspn(input, "\r\n")] = 0;
    strcpy(p.travail, input);

    return p;
}

int entreeChamp(char *verbe, int avecTous) {
    char *input = malloc(INPUT_SIZE);
    int compteur;

    do {
        printf("Quels champs voulez-vous %s ?\n", verbe);
        if (avecTous) printf("\n0] Tous les champs\n");
        printf("1] Prénom\n");
        printf("2] Nom\n");
        printf("3] Ville\n");
        printf("4] Code postal\n");
        printf("5] Téléphone\n");
        printf("6] Email\n");
        printf("7] Métier\n");

        scan(input);
        compteur = (int) strtol(input, NULL, 10);

        if (compteur < 0 || compteur > 7 || (!avecTous && compteur == 0))
            printf("\nERREUR: Entrez un champ correcte.\n");
    } while (compteur < 0 || compteur > 7 || (!avecTous && compteur == 0));

    return compteur;
}

struct Personne confirmEntree(char *verbe, char *phrase) {
    struct Personne p;
    int etat = 0;
    char *input = malloc(INPUT_SIZE);

    while (etat != 1) {
        p = entreePersonne(verbe);

        printf("\n%s | %s | %s | %s | %s | %s | %s | Est-ce bien les champs que vous voulez %s?\n0] Non\n1] Oui\n",
               p.prenom, p.nom, p.ville, p.codePost, p.tel, p.email, p.travail, phrase);

        scan(input);
        etat = (int) strtol(input, NULL, 10);

        if (etat == 1) {
            printf("\nAjouté.\n");
        } else if (etat == 0) {
            printf("\nAnnulé.\n");
        } else {
            printf("ERREUR DE SAISIE\n\n");
        }
    }

    return p;
}

void confirmGarder(struct Personne *pO, int *tO, struct Personne *pN, int tN) {
    char *input = malloc(INPUT_SIZE);
    int etat;

    do {
        printf("\nVoulez-vous remplacer le tableau par le résultat trouvé?\n0] Non\n1] Oui\n");
        scan(input);
        etat = (int) strtol(input, NULL, 10);

        if (etat == 1) {
            // replace tO by tN
            *tO = tN;
            memset(pO, 0, *tO * sizeof(struct Personne));
            memcpy(pO, pN, tN * sizeof(struct Personne));
        } else if (etat != 0) {
            printf("ERREUR DE SAISIE\n\n");
        }
    } while (etat != 0 && etat != 1);
}