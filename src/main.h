#define INPUT_SIZE 30

struct Recherche {
    struct Personne personne;
    int index;
};

char *compilerTable(struct Personne *table, int taille);

void afficherTable(struct Personne *table, int taille);

void afficherRecherche(struct Recherche *r, int taille);

struct Personne *filtrerTable(struct Personne *personnes, int *taille, char *);

struct Recherche *rechercherTable(struct Personne *personnes, int *taille, char *filtre);

struct Recherche *manquantsTable(struct Personne *personnes, int *taille, enum Champ parametre);